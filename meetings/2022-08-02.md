# Accessibility Working Group Meeting 2022-08-02

Document to record meeting discussions and outcomes. Meetings will be exported to the group's repo on Gitlab once it is set up. AWG meets bi- weekly on Tuesdays at 2:00PM GMT. Our next meeting takes place on August 16th, 2022 @ 2:00PM UTC (text-based).



## What are we?

We are a team of Fedora contributors who care about accessibility and digital inclusion for both Fedora Linux and the Fedora Community, with support and guidance from the Diversity, Equality, & Inclusion team. 



## Useful links

- [Jitsi Meet Room](https://meet.jit.si/fedora-accessibility-meeting)
- [HackMD](https://hackmd.io/team/fedora-accessibility-team?nav=overview)
- [Peertube](https://peertube.linuxrocks.online/c/fedora.accessibility/videos)
- [Gitlab](https://meet.jit.si/fedora-accessibility-meeting)
## Participation guidelines

- Please focus on listening, and on appreciating what others are trying to say, not only on what you are hearing.
- Wherever possible, please refrain from multitasking on email or social media and strive to remain fully present and tuned in to what others are saying and feeling.
- Please assume best intent in everyone’s comments and strive to keep a constructive tone in your own.
- Please use simple, accessible language. In particular, please avoid jargon and acronyms, so that all may fully participate.
- When you speak, please make just one point and then let others speak. We want everyone to have an equal chance to speak. In a group of “n” people, speak no more than “1/nth” of the time.
- Please speak for yourself when making comments, using “I”. Please don’t speak for the assembled group by speaking as “we”.



#### All meeting logs are available here [link to repo]



## Format

#### Chair

- 

#### Attendees

- ADD YOUR NAMES HERE

#### Agenda

- Topics here

---------------------------------


# 2022-08-02

#### Chair

- Emma Kidney
- Vipul Siddharth

#### Attendees

- Emma Kidney
- Vipul Siddharth
- Rikard
- Onuralp Sezer (FAS: thunderbirdtr)
- Sandipan Roy
- Luna Jernberg (bittin - 20 minutes late was at the Fedora GNOME meeting thats the same time)

#### Topics discussed
 * Open a ticket in DEI to discuss Accessibility survey - Emma 
 * Open a ticket with FPgM to get a program manager with us - Emma
 * Create a Infra ticket -> requesting FAS group for accessibility working group (will also help us authenticate to Gitlab) - Onuralp (Done)
     * https://pagure.io/fedora-infrastructure/issue/10839

 * Open a ticket with releng to research "enable assistive variables by default" posibility
 * Reach out to QA team (@sumantrom) and discuss accessibility test week - Vipul
     * File a ticket in QA (Examples: https://pagure.io/fedora-qa/issue/699
https://pagure.io/fedora-qa/issue/707 https://pagure.io/fedora-qa/issue/702)
 * Do a text based meeting - Vipul
 * Open a ticket to get help from a PM - Emma 

#### :pencil: Agenda

##### :watch: Kickoff

- [ ] Introductions :) 

##### :calendar: Administration

- [ ] - Establish meetings and communication 
    - Plan to only hold onto last four meetings as it is only used to catch up.
    - Meeting frequency? - Agreed last meeting to have meetings every two weeks. Option to change time if required to accommodate other members. Next meeting will take place on August 16th at 2:00pm UTC.
- [ ] - Repository / ticket tracker
    - Ticket filed with fedora infra to create gitlab group within fedora.
    - [Ticket filed with fedora-infrastructure team](https://pagure.io/fedora-infrastructure/issue/10820)
    - [Fedora's current GitLab group](https://gitlab.com/fedora)
    - Once repository is established, issues can be logged and a start on Fedora Accessibility documentation can begin.

##### :pushpin: Starting Tasks

- [ ] - Survey
    - Working in this document: https://hackmd.io/Ir-hLagdTHmTs4HgWB7uKg
    - Feel free to add to it!
    - Using Limesurvey 


- [ ] - Documentation
    - Pending repository
    

##### :open_hands: Other / Open Floor

- [ ] - 

###### tags: `Meeting` `August 2022`
